# Journée commune au GDR RSD, GPL (GT GLSEC) et GDR (GT SSLR)

## Thématique

Sécurité des piles réseau et de leurs implémentations

 - conception des protocoles
 - ingénierie logicielle pour le développement réseau
 - implémentation
 - tests
 - fuzzing
 - méthodes formelles
 - supervision


## Format de la journée

 - deux invités (idéalement un académique et un industriel)
 - des présentations de doctorants (avec appel à soumissions)
 - une session avec des interventions rapides (rump sessions)


## Actions

 - [X] fixer la date : lundi 30 septembre
 - [X] valider le lieu et réserver les salles : espace Dupanloup à Orléans
 - [ ] valider le format
 - [ ] envisager de la visio ? (question posée par qqun du bureau) Si oui, pour quelles interventions ?
 - [ ] monter la page web SciencesConf
 - [ ] recruter un comité de programme / d'organisation
 - [ ] finaliser la réservation
       => prévoir 70-80 personnes (salle Lecture) + salle cocktail + éventuellement
       une salle de réunion au cas où (salle bleue ou salle Europe)
 - [ ] lancer rapidement l'appel à soumissions pour les présentations de thèse
 - [ ] inviter les orateurs (1 académique et 1 industriel)
